package w5;

import java.util.Scanner;


public class Point {
    private double x;
    private double y;
    private String name;


    public Point(String name, double x, double y) {
        this.x = x;
        this.y = y;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Point " +  name + " {" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public double getDistance() {

        double Value = Math.pow(x, 2) + Math.pow(y, 2);
        double distance = Math.sqrt(Value);
        return distance;
    }

}
