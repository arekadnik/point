package w5;

import java.util.*;

public class Main3 {
    static List<Point> list = new ArrayList<Point>();

    public static void main(String[] args) {
        Comparator<Point> comparator = (Comparator<Point>) (p11, p21) -> (int) (p11.getDistance() - p21.getDistance());
        Scanner scanner = new Scanner(System.in);
        System.out.println("How many points do you want add");
        int number = scanner.nextInt();
        for (int i = 0; i < number; i++) {
            createPoint();
        }
        list.sort(comparator);
        printArray(list);
    }

    public static void createPoint() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Give Point name");
        String name = scanner.next();
        System.out.println("Set X");
        double x = scanner.nextInt();
        System.out.println("Set Y");
        double y = scanner.nextInt();
        Point point = new Point(name, x, y);
        list.add(point);
    }

    public static void printArray(List<Point> arrayList) {
        for (Point v : list) {
            System.out.println(v.toString());
        }
    }
}